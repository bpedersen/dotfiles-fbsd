#!/bin/sh

# install xorg keyboard layout for xorg
sudo ln -sf ~/dev/dotfiles-fbsd/freebsd/usr/local/etc/X11/xorg.conf.d/keyboard-dk.conf /usr/local/etc/X11/xorg.conf.d/keyboard-dk.conf

# xinit and resources
ln -sf ~/dev/dotfiles-fbsd/X/Xresources ~/.Xresources
ln -sf ~/dev/dotfiles-fbsd/X/xinitrc ~/.xinitrc

# wm config
mkdir -p ~/.config/i3
ln -sf ~/dev/dotfiles-fbsd/wm/i3/config ~/.config/i3/config

# emacs setup
mkdir -p ~/.emacs.d
ln -sf ~/dev/dotfiles-fbsd/emacs/init.el ~/.emacs.d/init.el
ln -sf ~/dev/dotfiles-fbsd/emacs/setup.el ~/.emacs.d/setup.el

# shell config
ln -sf ~/dev/dotfiles-fbsd/shell/bashrc ~/.bashrc

sudo cp ~/dev/dotfiles-fbsd/freebsd/etc/rc.conf /etc/rc.conf
sudo cp ~/dev/dotfiles-fbsd/freebsd/etc/sysctl.conf /etc/sysctl.conf
sudo cp ~/dev/dotfiles-fbsd/freebsd/boot/loader.conf /boot/loader.conf
