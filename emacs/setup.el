;;;;;;;;;;;;;;;;;;;;;;;;
;; use-package bootstrap
;;;;;;;;;;;;;;;;;;;;;;;;
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq use-package-always-ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;
;; packages setup
;;;;;;;;;;;;;;;;;;;;;;;;
(use-package helm
  :bind (("M-x" . helm-M-x)
         ("C-x C-f" . helm-find-files)
         ("C-x C-S-f" . helm-find)
         ("C-x C-b" . helm-mini)
         ("C-s" . helm-swoop)
         ("M-y" . helm-show-kill-ring)))

(use-package helm-swoop)

(use-package helm-google)

(use-package org
  :config
  (progn
    (setq org-time-clocksum-use-fractional t)
    (setq org-todo-keywords '((sequence "TODO(t)" "DOING(o)" "WAITING(w)" "|" "DONE(d)" "DELEGATED(e)")))
    (setq org-todo-keyword-faces '(("DELEGATED" . (:foreground "white" :weight "bold"))))
    (setq org-tag-alist '(("@work" . ?w) ("@home" . ?h) ("misc" . ?m)))
    (setq org-clock-into-drawer "CLOCK")
    (setq org-agenda-files '("~/.org/cal.org")))
  :bind ("C-c o c" . org-capture))

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package paredit
  :config
  (add-hook 'lisp-mode-hook #'paredit-mode)
  (add-hook 'scheme-mode-hook #'paredit-mode)
  (add-hook 'emacs-lisp-mode-hook #'paredit-mode)
  (add-hook 'emacs-mode-hook #'paredit-mode)
  (add-hook 'clojure-mode-hook #'paredit-mode))

(use-package beacon
  :config (beacon-mode))

(use-package magit)

(use-package cider
  :config
  (add-hook 'cider-mode-hook #'eldoc-mode))

(use-package company
  :config
  (global-company-mode)
  (setf company-idle-delay 0.5))

(use-package company-flx
  :config
  (company-flx-mode +1))

(use-package helm-company)

(use-package projectile
  :config (projectile-mode))

(use-package helm-projectile)

(use-package ace-jump-mode
  :bind (("C-+" . ace-jump-mode)
         ("C-*" . ace-jump-mode-pop-mark)))

(use-package geiser)

(use-package slime
  :config
  (progn
    (setq inferior-lisp-program "/usr/local/bin/sbcl --noinform")
    (if (file-exists-p (expand-file-name "~/quicklisp/slime-helper.el"))
        (load (expand-file-name "~/quicklisp/slime-helper.el"))
      )))

(use-package multiple-cursors
  :bind (("C-c C-a" . mc/mark-all-like-this)
         ("C-c C-l" . mc/edit-lines)))

(use-package dired+)

(use-package dracula-theme)

(use-package doom-themes)
;;;;;;;;;;;;;;;;;;;;;;;;
;; custom functions and setup
;;;;;;;;;;;;;;;;;;;;;;;;

(defun bf/disable-scrollbars-and-toolbars ()
  (menu-bar-mode -1)
  (when (display-graphic-p)
    (tool-bar-mode -1)
    (scroll-bar-mode -1)))

(defun bf/fix-osx-meta-key-bug ()
  (setq mac-option-modifier nil
        mac-command-modifier 'meta
        select-enable-clipboard t))

(defun bf/set-name-and-email (name email)
  (setq user-full-name name
        user-mail-address email))

(defun bf/enable-tab-completion ()
  (setq tab-always-indent 'complete)
  (add-to-list 'completion-styles 'initials t))

(defun bf/enable-spaces-for-indent ()
  (setq-default tab-width 4 indent-tabs-mode nil)
  (setq-default c-basic-offset 4 c-default-style "bsd"))

(defun bf/disable-splash-screen ()
  (setq inhibit-splash-screen t
        inhibit-startup-message t
        inhibit-startup-echo-area-message t
        inhibit-scratch-message nil))

(defun bf/disable-autosave-and-backup ()
  (setq make-backup-files nil
        auto-save-default nil))

(defun bf/set-font (name size)
  (let ((font (concat name "-" (number-to-string size))))
    (set-frame-font font nil t)))

(defun bf/misc-settings ()
  (setq display-time-24h-format t)
  (setq windmove-wrap-around t)
  (setq show-paren-delay 0)
  (display-time-mode)
  (windmove-default-keybindings)
  (global-linum-mode 1)
  (column-number-mode 1)
  (show-paren-mode 1)
  (global-set-key (kbd "C--") 'mode-line-other-buffer)
  (delete-selection-mode 1)
  (global-set-key (kbd "M-j")
                  (lambda ()
                    (interactive)
                    (join-line -1))))

(bf/disable-scrollbars-and-toolbars)
(bf/fix-osx-meta-key-bug)
(bf/set-name-and-email "Brian Bernt Frost" "brianfrostpedersen@gmail.com")
(bf/enable-tab-completion)
(bf/enable-spaces-for-indent)
(bf/disable-splash-screen)
(bf/disable-autosave-and-backup)
(bf/set-font "Droid Sans Mono" 11)
(bf/misc-settings)

(load-theme 'doom-one t)
;;(load-theme 'atom-one-dark t)


